

import React, { useState, useEffect } from 'react';

function App() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch("/get_data")
      .then(res => res.json())
      .then(response => {
        // Check if the "Data" property is an array
        if (Array.isArray(response.Data)) {
          setData(response.Data);
          console.log(response.Data);
        } else {
          console.error("Data received is not an array:", response);
        }
      })
      .catch(error => {
        console.error("Error fetching data:", error);
      });
  }, []);

  return (
    <div>
      {data.map((item, index) => (
        <h1 key={index}>{item}</h1>
      ))}
    </div>
  );
}

export default App;
