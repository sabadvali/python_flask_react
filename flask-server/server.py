from flask import Flask


app = Flask(__name__)


@app.route("/get_data")
def get_data():
    return {"Data":["data1","data2","data3","data4"]}

if __name__ == "__main__":
    app.run(debug=True)